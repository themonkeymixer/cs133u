// Author: Travis Wenks
// Title: String Processing
// Purpose: To accept string input
// Credit: Kavita, Nautiyal

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) 
{

    char str1[254], str2[254], str3[254];
    int len1, len2;

    printf("Enter string 1: ");
    fgets(str1, 254, stdin);

    printf("Enter string 2: ");
    fgets(str2, 254, stdin);

    len1 = strlen(str1);
    len2 = strlen(str2);

    printf("String 1 is %d bytes long, ", len1);
    printf("and String 2 is %d bytes long\n", len2);
    int str1lgn = len1 / 2;
    int str2lgn = len2 / 2;

    strncpy(str3, str1, len1 / 2);
    str3[len1 /2] = '\0';
    strcat(str3, str2 + len2 / 2);
    printf("String 3 is: %s\n", str3);

    return 0;

}
