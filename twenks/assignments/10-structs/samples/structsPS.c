#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define fileNameLength 256

struct distinctWords {
    char word[256];
    int lineNumber;
} wordArray[1000];

int main()
{
    FILE *inputFile;
    char inputFileName[256] = {0};
    int count = 0;
    int fileLine = 0;
    int index = 0;
    int testInt;
    char bufferWord[256];
    char bufferChar;
    char *result = NULL;
    char delims[] = " ";

    printf("Program: Structs\n");

    do
    {
        /*opening the file... same as array lab*/
        printf("Enter an input file name: ");
        fgets(inputFileName, fileNameLength, stdin);
        inputFileName[strlen(inputFileName) - 1] = '\0';
        if ((inputFile = fopen(inputFileName, "r")) == NULL)
        {
            printf("Unable to open \"%s\": %s\n", inputFileName,
                strerror(errno));
        }
    } while (inputFile == NULL);

    while (fgets(bufferWord, 256, inputFile) )          /* read one line at a time from the file */
    {
        ++fileLine;                                     /* increase the file number */
        printf("\nwords found in line %d, %s ",fileLine, bufferWord);
        bufferWord[strlen(bufferWord) -1] = '\0';       /* remove the linefeed char at the end */

        result = strtok( bufferWord, delims );          /* read until the first space (end of word) from line*/
        while( result != NULL ) {                       /* keep reading till no more words in the line*/
            printf( "\"%s\"\n", result );
            result = strtok( NULL, delims );
        }
    }
    fclose(inputFileName);
    return 0;

}