/*
  lot of scope to improve
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define fileNameLength 256

typedef struct  {
    char word[256];
    int lineNumber;
} word;

int main()
{
    FILE *inputFile;
    char inputFileName[256] = {0};
    int fileLine = 0;
    int wordsCountedSoFar = 0;      /* wordsCountedSoFar == 0 to begin with */
    int i;
    char bufferWord[256];
    char bufferChar;
    char *result = NULL;
    char delims[] = " ";
    int foundWord = 0;
    word Dictionary[1000];
    printf("Program: Structs\n");

    do
    {
        /*opening the file... same as array lab*/
        printf("Enter an input file name: ");
        fgets(inputFileName, fileNameLength, stdin);
        inputFileName[strlen(inputFileName) - 1] = '\0';
        if ((inputFile = fopen(inputFileName, "r")) == NULL)
        {
            printf("Unable to open \"%s\": %s\n", inputFileName,
                strerror(errno));
        }
    } while (inputFile == NULL);

    for(i = 0; i < 1000; ++i)
    {
		/* 
		 * initialize the line numbers for each word to be 0
		 * that way later on when I've collected all the words
		 * I will filter the ones with line number not equal to 0
		*/
        Dictionary[i].lineNumber = 0;
    }
    while (fgets(bufferWord, 256, inputFile) )          /* read one line at a time from the file */
    {
        ++fileLine;                                     /* increase the file number */
        //printf("\nwords found in line %d, %s ",fileLine, bufferWord);
        bufferWord[strlen(bufferWord) -1] = '\0';       /* remove the linefeed char at the end */

        result = strtok( bufferWord, delims );          /* read until the first space (end of word) from line*/
        while( result != NULL ) {                       /* keep reading till no more words in the line*/
            printf( "\"%s\"\n", result );

            /** store the distinct words **/
            for(i = 0; i < wordsCountedSoFar; ++i)      /* wordsCountedSoFar == 0 to begin with
                                                        */
            {
                if(strcmp(Dictionary[i].word, result) == 0) /* if the word found in the dictionary, exit from this 
															   particular word loop.
															   Because this word is not distinct, I do not need to
															   store it
															 */
                {                               
                    break;
                }
            }                                               /* else iterate through the entire loop */

            strcpy(Dictionary[i].word, result);            /* if i is equal to wordsCountedSoFar, means word
                                                              was not found in the dictionary,
                                                              else if i < wordsCountedSoFar, word already exists
                                                              so don't increase the word count
                                                            */
            if(wordsCountedSoFar <= i + 1)
            wordsCountedSoFar = i + 1;
            if( Dictionary[i].lineNumber == 0)
                Dictionary[i].lineNumber = fileLine;

            /** store the distinct words **/
            result = strtok( NULL, delims );
        }
    }
    fclose(inputFile);                               /* VERY IMPORTANT TO CLOSE THE FILE PTR*/

/* PRINT the words in the line they occured first */
    for(i = 0; i < 1000; ++i)
    {
        if(Dictionary[i].lineNumber > 0)
            printf("word %s found at line:%d\n", Dictionary[i].word, Dictionary[i].lineNumber);
    }
    return 0;

}