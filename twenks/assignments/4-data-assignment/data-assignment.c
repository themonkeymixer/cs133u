// Author: Travis Wenks
// Title: Data Assignment
// Purpose: declare and print variables
// Credits: Daniel Nibler

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

int main(void)
{

    char str1[] = "Hello World! I am a string!";
    //declare and store string

    char chr1;
    //declare character

    chr1 = 'a';
    //define variable

    int int_shrt_max = SHRT_MAX;
    //maximum integer in short data type

    double inf1 = HUGE_VAL;
    //double precision intinity

    int *p;
    //declare assignmet

    p = &int_shrt_max;
    //assigns p to int_shrt_max

    printf("Print test string - \t\t\t\t %s\n", str1);
    //reads the value of the variable

    printf("Print test letter as c - \t\t\t %c\n", chr1);
    //reads the value of the variable

    printf("Print test letter as d - \t\t\t %d\n", chr1);
    //reads the value of the variable

    printf("Print max int value - \t\t\t\t %d\n", int_shrt_max);
    //reads the value of the variable

    printf("Print infinity - \t\t\t\t %f\n", inf1);
    //reads the value of the variable

    printf("Print the value stored in the pointer - \t %d\n", p);
    //reads the value of p but not the target

    printf("Print the value of the short int - \t\t %d\n", *p);
    //reads the value of the target

    return 0;
}
