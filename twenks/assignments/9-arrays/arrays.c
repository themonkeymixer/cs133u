// Author: Travis Wenks
// Title: Arrays
// Purpose: Accepting and manipulating user supplied files
// Compile command: gcc -Wextra -pedantic arrays.c -o array -lm
// Credit: arrays.c doodle

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

<<<<<<< HEAD
int main(int argc, char *argv[])
{
    
    FILE *pFile;
    char name[21];
    int score;
    
    //open the file
    pFile = fopen("database.txt", "w");
    
    if(pfile != NULL)
    {
    	while ()
    	{
			fscanf(pFile, "%s %d", name, &score);
			
			printf("Are there any more students [y/n] ");
			answer = getchar();
			 	
    		}
    	
    
    	fclose(pfile);
    	}
    
    else
    {
    	printf("could not open file. \n");
    	
    int i;
	i = 1;
	while (i < n)
	  i = i * 2;
	  
	  
=======
int main(void)
	{

	//array for ascii charecters
	int asciiArray[128] = { 0 };
	char inCharacter = '\0';

	//source file
	char user_file[256];

	//loop counter
	int loop_number = 0;

	//ceasar chift for encrpytion
	unsigned short int ceasar_encrypt = 0;

	//number of characters read from user file
	int file_char_len = 0;

	//frequency of the charecter in the user file
	float char_freq = 0;

	//total entropy in user_file
	float file_entropy = 0;

	//input and output files
	FILE * inputFile = 0;
	FILE * outputFile = 0;

	//get file
	do
	{
		printf("Enter an input file name: ");
		fgets(user_file, 256, stdin);

		user_file[strlen(user_file)-1] = '\0';

		if(!(inputFile = fopen(user_file,"r")))
		{
			printf("Source not readable please reenter %s\n\n", user_file);
		}

	} while (inputFile==NULL);

	printf("Enter an unsigned short int : ");
	scanf("%hu",&ceasar_encrypt);

	printf("\nThe input string is\n\n");	

	while((inCharacter = fgetc(inputFile)) != EOF)
	{
		++file_char_len;
		printf("%c", inCharacter);
		++asciiArray[inCharacter];
	}


	printf("\n\n\nTotal characters entered : %d \n\n",file_char_len);
	
	printf("The character frequencies for %s are:\n\n", user_file);

	while(loop_number < 128)
	{
		if(asciiArray[loop_number]!=0)
		{
			char_freq = (float) asciiArray[loop_number] / (float) file_char_len;
			file_entropy += (float) (char_freq * (log(char_freq) / log(2)));
			printf("'%c' occurs %d times for a relative frequency of %.3f\n", loop_number, 
				asciiArray[loop_number], char_freq);
		}
		++loop_number;
	}
	 
	file_entropy *= -1;
	printf("\nThe per character entropy is : %.3f\n", file_entropy);
	printf("The total entropy is         : %.3f\n", (file_entropy * file_char_len));

	//close file pointer
	fclose(inputFile); 

	if(!(inputFile = fopen(user_file,"r")))
	{
		printf("Reading supplied file failed!");
		
		//os independant pause 
		//credit: http://faq.cprogramming.com
		printf ("Press [Enter] to continue . . .\n" );
		fflush (stdout);
		getchar();

		return 0;
	}

	/* We then open our output file for writing, with a "w", this will error our if
	 * file already exists, so we have to check that too
	 */
	if(!(outputFile = fopen("out.txt","w")))
	{
		printf("\nCan't open output.txt for writing, maybe file already exists?\n");
		
		//os independant pause 
		//credit: http://faq.cprogramming.com
		printf ("Press [Enter] to continue . . .\n" );
		fflush (stdout);
		getchar();

		return 0;
	}

	while((inCharacter = fgetc(inputFile)) != EOF)
	{
		if(inCharacter > 64 && inCharacter < 91) /* >= 'A' && <= 'Z' */
		{
			inCharacter += ceasar_encrypt;;
			if(inCharacter > 90) /* > 'Z' */
			{
				inCharacter -= 26;
			}
			fprintf(outputFile,"%c", inCharacter);
		}
		else if(inCharacter > 96 && inCharacter < 123) /* >= 'a' && <= 'z' */
		{
			inCharacter += ceasar_encrypt;;
			if(inCharacter > 122) /* > 'Z' */
			{
				inCharacter -= 26;
			}
			fprintf(outputFile, "%c", inCharacter);
		}
		else
		{
			fprintf(outputFile, "%c", inCharacter);
		}
	}

	printf("\nThe file out.txt has been written with a Caesar shift of %d\n\n",ceasar_encrypt);

	fclose(outputFile);
	fclose(inputFile);
	
	//os independant pause 
	//credit: http://faq.cprogramming.com
	printf ("Press [Enter] to continue . . .\n" );
	fflush (stdin);
	getchar();
>>>>>>> 1328387926277a20528e9597603cc61c0c2a3793
	return 0;
}
