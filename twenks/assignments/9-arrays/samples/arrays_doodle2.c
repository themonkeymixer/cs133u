#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define fileNameLength 256

int main() {

    //Local variables
    char inputFileName[fileNameLength] = {0};         //Empty input file name
    char inByte;                                      //Byte from input file
    char start;
    FILE * inputFile;                                 //Input file stream
    int freqTable[fileNameLength] = {0};                         //Frequency table, all initialized to 0
    int  idx;                                         //Loop variable
    int totalChars = 0;
    //Get a file name, and then open the file                                        //Loop variable
    unsigned short int cipher;
    //Get a file name, and then open the file
    do {
        printf("Enter input file name: ");                //Prompt
        fgets(inputFileName,fileNameLength,stdin);        //Read the file name
        /*
        My File name with full path is
        C:\Users\Public\Documents\kavita1.txt
        which I enter as
        C:\\Users\\Public\\Documents\\kavita.txt
        because \ in C in a string stands for escape
        ex: \n for a new line, \t for tab
        */
        inputFileName[strlen(inputFileName) - 1] = '\0';  //Kill the \n
       
        if((inputFile = fopen(inputFileName,"r")) == NULL) {
            fclose(inputFile);
            printf("Unable to open \"%s\": %s\n",
                inputFileName,strerror(errno));
        }

    } while(inputFile == NULL);

    printf("Enter an unsigned short integer for cipher: ");
    scanf("%u", &cipher);
    /*
    Read from the file char by char -> fgetc(inputFile)
    store current read character in inByte -> (inByte = fgetc(inputFile))
    Compare inByte to check whether it is EOF(End of File) -> (inByte = fgetc(inputFile)) != EOF
    if not enter the loop body and execute the statement in the body

    */
    while((inByte = fgetc(inputFile)) != EOF) {
        ++freqTable[inByte];                   //Count the byte
        ++totalChars;                          // total characters read so far
        /*
        So if the M is the current character in inByte, ascii code 77
        freqTable[inByte] which is freqTable[77] is incremented
        */
        if(inByte >= 'A' && inByte <= 'Z')
        {
            start = inByte - 'A';
            putchar( (start + cipher) % 26 + 'A'); /* so that it remains within A-Z*/
        }
        else if(inByte >= 'a' && inByte <= 'z')
        {
            start = inByte - 'a';
            putchar( (start + cipher) % 26 + 'a'); /* so that it remains within a-z*/
        }
        else
        {
            putchar(inByte);
        }
    }
    fclose(inputFile);                                //Close the file  ALWAYS REMEMBER TO CLOSE AN OPENED FILE HANDLE

    /* Printing the answer */
    printf("\n\n");

    for(idx = 0; idx < 256; idx++) {

        if(freqTable[idx] > 0) /* need only the chars found in the file, hence the ones with count > 0*/
        {
            printf("Char code %03d ",idx); /* no new line, printing the ascii value of the character */
            switch((char)idx) { /*convert idx to char, which'll give me the literal value of the character*/
                    case '\r':
                        printf("(\\r"); /*special*/
                        break;
                    case '\n':
                        printf("(\\n");
                        break;
                    case '\t':
                        printf("(\\t");
                        break;
                    case '\v':
                        printf("(\\v");
                        break;
                    default:
                        printf(" (%c",idx);
                        break;
            }
            printf(") occurs %d times\n",freqTable[idx]);
        }
    }
    return 0;
}
