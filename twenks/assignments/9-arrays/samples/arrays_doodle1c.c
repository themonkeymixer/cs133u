/*

Following is a steroid-less version of the array lab. It is incomplete and different in places from the lab

, but adequate to give you a vital points' start

It reads a user entered line of text from stdin(console window), not a file as required in the lab

counts the total number of each character found in the sentence

as well as displays the frequency and percentage of each

you can test it to get a feel of how the array is being used to store the frequency as

well as the characters occuring in the user entered sentence

*/

#include <stdio.h> /* Directors - Standard Input / Output */
#include <stdlib.h> /* System Library */

int main() /* The one and only one Main... */

{
/* Define and initialize variable(s) */
char textChar; /* Character variable to count text and load ASCII array */
int textLenght = 0; /* Counters the number of characters in the entered text line */

/* Define and initialize array(s) */
/* Array which stores the frequency of the the characters in the entered text
line that corresponds with its ASCII value.*/
int asciiArray[128] = {0};
int i;                    /* Loop counter */



/* This code works by first initializing an array that is the size of the
standard ASCII table (0-127 characters) and setting all the elements to
zero. The entered text is then pulled one character at a time using
getchar() in a "while" loop. The array element that corresponds with
that character's ASCII value is incremented by one along with a counter
called textLength that tracks the number of entered characters. The
frequency table headers, textLength, and the percentage of all the
characters printed. Then a "for" loop incremented by the 128 elements
in the standard ASCII table uses a condition to check if the value of
the array element is not zero. If true, the text character, the frequency
the percentage (by dividing the element by textLength) is printed. */


/* Enter the text, fill the array and count the characters */
printf("Enter a line of text: ");


/* Start loop, continues until if hits CR */
while ((textChar = getchar())!= '\n') {
textLenght++; /* Counts each character */
asciiArray[textChar]++; /* Increments array element that corresponds with the character's ASCII value */

}
/* now print the result*/
     printf("\nFREQUENCY TABLE\n");
     printf("---------------\n");
     printf("Char Count %% of Total\n");
     printf("---- ----- ----------\n");
    
     /* Prints the summation data outside of loop that checks the array */
     printf(" ALL %5d %9.2f%%\n", textLenght,( textLenght * 100.0 ) / textLenght );
    
    
     /* The "for" loop checks each element of the array for a non-zero value and
        then prints the frequency value if true.  The loop increments correspond
        with the values of a standard ASCII table. */
     for (i = 0; i < 128; i++)
         if (asciiArray[i] > 0)
            printf(" \"%c\" %5d %9.2f%%\n", i, asciiArray[i], ( asciiArray[i] * 100.0 ) / textLenght );        
           
        
 
    /* Wait for user input to terminate program  but remove when submitting*/
    system("pause");
    return 0;
}
