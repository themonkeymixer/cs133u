while((input = fgetc(myFile)) != EOF)
    {
        ++asciiCharTable[input];
        fileArray[totalChars] = input;
        ++totalChars;
    }
    fileArray[totalChars] = '\0';   //add NULL to mark end of string

    fclose(myFile);

    printf("\nCharacter | Absolute Freq.| Relative Freqency\n");
    for(asciiVal = asciiStart; asciiVal < asciiEnd; ++asciiVal)
    {
        if ( asciiCharTable[asciiVal] != 0)
        {
            absFrequency    = asciiCharTable[asciiVal];
            relFrequency    = (double)asciiCharTable[asciiVal] / totalChars;
            subEntropy      = relFrequency * log(relFrequency) / log(2.0);
            switch ((char)asciiVal)
            {
            case '\r':
                printf("  '\\r'");
                break;
            case '\n':
                printf("  '\\n'");
                break;
            case '\t':
                printf("  '\\t'");
                break;
            case '\v':
                printf("  '\\v'");
                break;
            default:
                printf("  '%c'", asciiVal);   //print the char
                break;
            }
            printf("\t\t%4d \t     %5.3f \n", absFrequency, relFrequency);

            perCharEntropy += subEntropy;
        }
    }

    perCharEntropy *= -1.0;

    printf("\nThe per-character entropy is %5.3f\n", perCharEntropy);
    printf("The total entropy is %5.3f\n", perCharEntropy * totalChars);
