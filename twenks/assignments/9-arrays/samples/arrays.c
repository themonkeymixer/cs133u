/* Filename: arrays.c
 * Author  : 
 * Class   : 
 *
 * Date    : 
 *
 * This program reads in a file, counts the characters in the file, counts
 * the number of each character in the file.  It then calculates the percentage
 * that each character occurs, and the per character entropy and total entropy
 * of the input
 *
 * entropy = ( (%charOccurs) * log2(%charoccurs) + the same for every character) * -1
 * total entropy = entropy * total number of characters
 *
 * This program then takes the input file, and does a caesar shift of whatever the value
 * for an unsigned short int that the user enters and puts the caesar shifted file in
 * a file called out.txt
 *
 * It does not shift non alphabetic characters, and it shifts lowercase characters to
 * lowercase and uppercase characters to uppercase.  If it shifts a Z it will began
 * again at the beginning of the alphabet to complete the shift.
 * IE: Z + 2 shift = B or z + 2 shift = b
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main()
{
	/* Here we set up an array to keep a count of each character read in, ascii has values
	 * 0 - 127 so a 128 sized array works perfect.
	 * The inCharacter variable is where we store characters read in from the file one
	 * by one, we also use this variable to write to the output file
	 */
	int asciiArray[128] = { 0 };
	char inCharacter = '\0';
	
	/* This nifty variable keeps count in any loops we do
	*/
	int counter = 0;
	
	/* This is the value of our caesar shift
	*/
	unsigned short int shift = 0;

	/* inputLength is the total number of characters read in, and frequency is a 
	 * variable we use to calculate the per character frequency.
	 * Entropy is the variable we calculate our entropy with
	 */
	int inputLength = 0;
	float frequency = 0;
	float entropy = 0;

	/* File descriptors, one for our input and one for our output
	*/
	FILE * inputFile = 0;
	FILE * outputFile = 0;

	char fileName[256];

	printf("Program : arrays.c\n");
	printf("Author  : \n\n");

	/* We prompt for an input file until we get a valid one, we have to make
	 * sure we've opened a real file descriptor lest we try to read from
	 * an invalid one
	 */
	do
	{
		printf("Please enter a filename : ");
		fgets(fileName, 256, stdin);

		fileName[strlen(fileName)-1] = '\0';

		if(!(inputFile = fopen(fileName,"r")))
		{
			printf("Unable to open : %s\n\n", fileName);
		}

	} while (inputFile==NULL);

	printf("Enter an unsigned short int : ");
	scanf("%hu",&shift);

	printf("\nInput String : ");
	
	/* Heres where the magic happens:
	 * We read out input file one character at a time, we increment our count
	 * every time we read a character, and we keep track of the number of times our
	 * character was read in in our array
	 * We also use this oppurtunity to output the input we've read in so the
	 * user can see the original string that was read in
	 */
	while((inCharacter = fgetc(inputFile)) != EOF)
	{
		++inputLength;
		printf("%c", inCharacter);
		++asciiArray[inCharacter];
	}
	printf("\n\n");

	printf("Total characters entered : %d \n\n",inputLength);
	
	/* Here we go back through our array to determine some special values!
	 * We also use our nifty loop counter variable.
	 * By taking the number of times a character occurs and dividing it by
	 * the total number of characters we get out frequency,
	 * then we increment our entropy (calculation shown at top of file)
	 * and we take the oppurtunity to print a character by character analysis
	 * of how many times a character occurs and it's frequency
	 */
	printf("The character frequencys for \"%s\" are : \n\n", fileName);

	while(counter < 128)
	{
		if(asciiArray[counter]!=0)
		{
			frequency = (float) asciiArray[counter] / (float) inputLength;
			entropy += (float) (frequency * (log(frequency) / log(2)));
			printf("'%c' occurs %d times for a relative frequency of %.3f\n", counter, 
				asciiArray[counter], frequency);
		}
		++counter;
	}
	 
	entropy *= -1;
	printf("\nThe per character entropy is : %.3f\n", entropy);
	printf("The total entropy is         : %.3f\n", (entropy * inputLength));

	/* Gotta close our files */
	fclose(inputFile); 

	/* We reopen our file from the begginning to do our caesar shift,
	 * potentially we could have done this at the top, but it seems less terse to
	 * do it here.
	 * We also double check that we can still open said file, someone might
	 * have been sneaky and moved it.
	 */
	if(!(inputFile = fopen(fileName,"r")))
	{
		printf("Ouch how did this not work the second time!");
		system("PAUSE");
		return 0;
	}

	/* We then open our output file for writing, with a "w", this will error our if
	 * file already exists, so we have to check that too
	 */
	if(!(outputFile = fopen("out.txt","w")))
	{
		printf("\nCan't open output.txt for writing, maybe file already exists?\n");
		system("PAUSE");
		return 0;
	}

	/* We read our file in one character at a time again, and decide whether to
	 * write the same character we read in out, or to shift an alphabetic character
	 *
	 * There are many ways to do this, but I did ascii arithmetic to do mine, I check
	 * if the character is an uppercase character, if it is I increment it by the shift
	 * then I make sure it is still in the range of an uppercase character, if not
	 * I shift it back to the begginning of the alphabet, shifting Z's to a capital
	 * letter at the beginning of the alphabet.
	 *
	 * I then do the same for lowercase letters, and if it is not an uppercase or lowercase
	 * letter we just write it out as is
	 */
	while((inCharacter = fgetc(inputFile)) != EOF)
	{
		if(inCharacter > 64 && inCharacter < 91) /* >= 'A' && <= 'Z' */
		{
			inCharacter += shift;
			if(inCharacter > 90) /* > 'Z' */
			{
				inCharacter -= 26;
			}
			fprintf(outputFile,"%c", inCharacter);
		}
		else if(inCharacter > 96 && inCharacter < 123) /* >= 'a' && <= 'z' */
		{
			inCharacter += shift;
			if(inCharacter > 122) /* > 'Z' */
			{
				inCharacter -= 26;
			}
			fprintf(outputFile, "%c", inCharacter);
		}
		else
		{
			fprintf(outputFile, "%c", inCharacter);
		}
	}

	/* Let the user know where we wrote out file, and close our file descriptors
	 * and we're done
	 */
	printf("\nThe file out.txt has been written with a Caesar shift of %d\n\n",shift);

	fclose(outputFile);
	fclose(inputFile);

	system("PAUSE");
	return 0;
}
