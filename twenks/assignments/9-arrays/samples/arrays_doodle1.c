#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define fileNameLength 256

int main() {

    //Local variables
    char inputFileName[fileNameLength] = {0};         //Empty input file name
    FILE * inputFile;                                 //Input file stream
    int freqTable[fileNameLength] = {0};                         //Frequency table, all initialized to 0
    //Get a file name, and then open the file
    do {
        printf("Enter input file name: ");                //Prompt
        fgets(inputFileName,fileNameLength,stdin);        //Read the file name
        /*
        My File name with full path is
        C:\Users\Public\Documents\kavita1.txt
        which I enter as
        C:\\Users\\Public\\Documents\\kavita.txt
        because \ in C in a string stands for escape
        ex: \n for a new line, \t for tab
        */
        inputFileName[strlen(inputFileName) - 1] = '\0';  //Kill the \n

        if((inputFile = fopen(inputFileName,"r")) == NULL) {
            fclose(inputFile);
            printf("Unable to open \"%s\": %s\n",
                inputFileName,strerror(errno));
        }

    } while(inputFile == NULL);
    fclose(inputFile);                                //Close the file  ALWAYS REMEMBER TO CLOSE AN OPENED FILE HANDLE
    return 0;
}
