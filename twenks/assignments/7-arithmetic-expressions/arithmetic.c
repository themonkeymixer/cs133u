// Author: Travis Wenks
// Title: Arithmetic Expressions
// Purpose: To creating, manipulating, and displaying arithmetic expressions
// Compile command: gcc -Wall arithmatic.c -o arithmatic -lm

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>


int main(void)
{
    unsigned short shrt1, shrt2, shrt3, shrt4, shrt5;
    float flt1, flt2, flt3, flt4;

    //#1 get short integer
    printf("Enter unsigned short int: ");
    scanf("%hu", &shrt1);

    //calculate #2
    //shrt2 = unsigned short pow(unsigned short 2 , unsigned short shrt1);
    shrt2 = pow(2 , shrt1);
    
    //#2 print the exponent
    printf("The value of 2 raised to the %hu power is %hu\n", shrt1, shrt2);
    
    
    //#3 get short integer #2 , radius of a circle
    printf("Enter unsigned short int: ");
    scanf("%hu", &shrt3);

    //#4 calculate circumference of a circle
    shrt4 = 2 * M_PI * shrt3;
    shrt5 = M_PI * shrt3 * shrt3;

    //#4 print calculated values
    printf("\nA circle with radius %hu",shrt3); 
    printf(" has circumference of %hu",shrt4);
    printf(" and an area of %hu \n",shrt5);

    //#5 polynomial
    printf("Enter a float value:");        
    scanf("%f", &flt1);

    //#5 calculate
    flt2 = 2 * flt1 * flt1 * flt1;
    flt3 = 3 * flt1 * flt1;
    flt4 = 4 * flt1 + 5 + flt2 + flt3;

    //#5 display
    printf("\nThe value of 2(%f)**3 +", flt1); 
    printf(" 3(%f)**2 + 4(%f) + 5", flt1, flt1);
    printf(" = %f\n\n\n", flt4);
  
    return 0;

}
