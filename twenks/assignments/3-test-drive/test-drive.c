// Author: Travis Wenks
// Title: Test Drive
// Purpose: To print the answers to the intro questions

#include <stdio.h>
#include <stdlib.h>
int main() {

    printf("\n\nAuthor: Travis Wenks\n");
    printf("Title: Test Drive\n\n");

    printf("1.  Travis Wenks g00630719\n\n");
    printf("2.  Required for engr transfer\n\n");
    printf("3.  debugging linux scripts and running MySQL database\n\n");
    printf("4.  Debian 3.0.0-1 Kernel Laptop GCC v4:4.6.1-2\n\n");
    printf("5.  I use vim + gcc with auto tabs and coloration,\n");
    printf("      I tried anjuta but didn't like it very much\n\n");
    printf("6.  Getting my brain to go from debugging to invention\n\n");
    printf("7.  Learning c, I want to be able to commit to the kernel\n\n");
    printf("8.  I want to earn an a\n\n");
    printf("9.  I have no idea\n\n");
    printf("10. Sylvania\n\n\n");

    return 0;

           }
