// Author: Travis Wenks
// Title: Selection
// Purpose: To creating and manipulating selection control structures
// Compile command: gcc -Wall arithmatic.c -o arithmatic -lm
// Credits: Daniel Nibler

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(void)
{
    //operators
    char x, y, z;
    //operands
    int a = 0, b = 0,c = 0, d = 0;
    //operator categories
    char Category1[] = "<>";
    char Category2[] = "+-*%/";
    char Category3[] = "|&";

    char resultAns[10];
    int whichCategory = 0;
    
    //flag to check which category the operators have fallen into
    //if it stays 0, means the operators are not from the same category
    //results
    int result1 = 0, result2 = 0, result3 = 0;

  
    printf("Enter an expression: ");
    scanf("(%d %c %d) %c (%d %c %d)", &a, &x, &b, &y, &c, &z, &d);

    if ((strchr(Category1, x) != NULL) && \
        (strchr(Category1, y) != NULL) && \
        (strchr(Category1, z) != NULL)) 
        {
            whichCategory = 1;
            puts("\nValid input. These are all relational  operators...");
        }

    else if ((strchr(Category2, x) != NULL) && \
         (strchr(Category2, y) != NULL) && \
         (strchr(Category2, z) != NULL)) 
         {
             whichCategory = 2;
             puts("\nValid input. These are all arithmetic operators...");
        }

    else if ((strchr(Category3, x) != NULL) && \
         (strchr(Category3, y) != NULL) && \
         (strchr(Category3, z) != NULL)) 
         {
             whichCategory = 3;
             puts("\nValid input. These are all logical operators...");
        }

    if(whichCategory == 0)  
    // if the operators are not from the same category
    {
        printf("Result: (%d %c %d) %c (%d %c %d) is not a valid expression for this program\n", a, x, b, y, c, z, d);
        return 0;
        //exit with message if the operators are not from the same category
    }

//will reach here only the operators are  from the same category
//(a x b) y (c z d)
//result1 = a op b
//result2 = c op d
//result3 = result1 op result2
    
    //for "x"
    switch(x)
    {
        //catagory1
        case '<': result1 = ( a < b );
        break;      
        case '>': result1 = ( a > b );
        break;
         
         //catagory2
        case '+': result1 = ( a + b);
        break;   
        case '-': result1 = ( a - b);
        break;
        case '*': result1 = ( a * b);
        break;
        case '%': result1 = ( a % b);
        break;
        case '/': result1 = ( a / b);
        break;
        
         //catagory3  
        case '|': result1 = ( a || b);
        break;
        case '&': result1 = ( a && b);
        break;
    }
   
    //for "z"
    switch(z)
    {
        //catagory1
        case '<': result2 = ( c < d);
        break;
        case '>': result2 = ( c > d);
		break;
		
        //catagory2
        case '+': result2 = ( c + d);
        break; 
        case '-': result2 = ( c - d);
        break;
        case '*': result2 = ( c * d); 
        break;
        case '%': result2 = ( c % d);
        break;
        case '/': result2 = ( c / d);
        break;

        //catagory3
        case '|': result2 = ( c || d);
        break;
        case '&': result2 = ( c && d);
        break;
    }

    //for "y"
    switch(y)
    {
        //catagory1
        case '<': result3 = (result1 < result2);
        break;
        case '>': result3 = (result1 > result2);
        break;
        
        //catagory2
        case '+': result3 = (result1 + result2);
        break;
        case '-': result3 = (result1 - result2);
        break;
        case '*': result3 = (result1 * result2);
        break;
        case '%': result3 = (result1 % result2);
        break;
        case '/': result3 = (result1 / result2);
        break;
        
        //catagory3
        case '|': result3 = (result1 || result2);
        break;
        case '&': result3 = (result1 && result2);
        break;
    }
    
	if(whichCategory == 2)  
	// arithmetic
	{
		printf("Result: (%d %c %d) %c (%d %c %d) = %d\n", a, x, b, y, c, z, d, result3);
	}
	else
		if(whichCategory == 1 || whichCategory == 3)  
		// if the operators are not from the same category
		{
		if(result3 != 0)
		{
			strcpy(resultAns,"true");
		}
	
		else
			strcpy(resultAns,"false");
			printf("Result: (%d %c %d) %c (%d %c %d) = %s\n", a, x, b, y, c, z, d, resultAns);
			
		}

 
	return 0;  
}


