// Author: Travis Wenks
// Title: Hello World
// Credits: Original Source, Nautiyal, Kavita
// Purpose: To print "Hello World"


/*
Hello World demonstration program
Winter 2011 - CS 133U
*/

#include <stdio.h>
#include <stdlib.h>
int main() {
        printf("Hello World!\n");
            return 0;
}
