// Author: Travis Wenks
// Title: Data Assignment
// Purpose: accept and print formatted input
// Credits: Daniel Fellin


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define STR_LEN 80 //this idea from daniel fellin

int main() 
{
    //define variables
    char chr1, chr2, chr3, chr4;
    int in1, in2, in3;
    float flt1, flt2, flt3;
    char str1[STR_LEN+1];

    //number 1+2
    printf("Enter char int char float: ");
    scanf("%c %d %c %f", &chr1, &in1, &chr2, &flt1);
    printf("You entered: %c %d %c %f\n", chr1, in1, chr2, flt1);

    //number 3+4
    printf("Enter string char float int char: ");
    scanf("%s %c %f %i %c", str1, &chr3, &flt2, &in2, &chr4);
    printf("You entered: %s %c %f %i %c\n", str1, chr3, flt2, in2, chr4);    

    //number 5+6
    printf("Enter an integer value: ");
    scanf("%d", &in3);
    printf("You entered: %015d\n", in3);
    
    //number 7+8
    printf("Please enter a float: ");
    scanf("%f", &flt3);
    printf("You entered: %15.2f\n\n\n", flt3);
    
    return 0;
}

