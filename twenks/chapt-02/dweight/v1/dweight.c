// Name: Dimensional Weight
// Purpose: Calculate the dimensional weight of a box
// Author: Travis Wenks
// Source Credit: K.N. King

#include <stdio.h>

int main (void)
{

 int height, length, width, volume, weight;

 height = 8;
 length = 12;
 width = 10;
 volume = height * width * length;
 weight = (volume + 165) / 166;

 printf("Dimensions: %dx%dx%d\n", length, width, height);
 printf("Volume (cubic inches): %d\n", volume);
 printf("Dimensional weight (pounds): %d\n", weight);

 return 0;
}

