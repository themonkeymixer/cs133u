// Title: Sphere Volume calculator
// Purpose: Calculate the volume of a sphere based on inputed dimensions
// Author: Travis Wenks

#include <stdio.h>

int main(void)

{
    float radius, pi = 3.1415926536;

    printf("This will calculate the volume of a sphere\nusing the first ten digits of pi,\nthis is adequate to remove any rounding from the measurement.\n");

    printf("Enter sphere radius, accurate to greater than 1 decimal place eg 5.5, or 5.005:");
    scanf("%f", &radius);

    printf("The cubic volume is: %f\n", radius * radius *radius * pi * 4.0f / 3.0f);

    printf("\n\n\n        *\n       *\n      *\n *   *\n  * *\n   *\n");
    return 0;
}

    
