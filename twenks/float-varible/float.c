#include <stdio.h>

int main()
{
   float age = 10.5, load = 1.4;

   printf("TechOnTheNet.com is over %f years old and pages load in %f seconds.\n", age, load);
   
   return 0;
}
