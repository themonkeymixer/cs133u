#include <stdio.h>

// Name: Travis Wenks
// Purpose: To show how several scanf and printf statments work
// Credits: K. N. King

int main(void)
{
	// printf printing on a single line
	printf("To C, or not to C: that is the question. \n");

	// printf using 2 lines
	printf("Brevity is the soul of wit. \n  --Shakespeare \n");

	

	return 0;
}


