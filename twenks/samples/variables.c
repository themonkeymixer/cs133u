#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int num1, num2, sum;
    
    printf("\nPlease enter the first number.");
    scanf("%d", &num1);
    
    printf("\nPlease enter the second number.");
    scanf("%d", &num2);
    
    sum = num1 + num2;
    
    printf("the sum is %d\n\n", sum);
    
    return 0;

}
