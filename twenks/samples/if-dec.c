#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    int num1, num2;
    
    printf("\nPlease enter the first number.");
    scanf("%d", &num1);
    
    printf("\nPlease enter the second number.");
    scanf("%d", &num2);
    
    if(num1 == num2)
    {
        printf("They are equal\n");
    }
    
    if(num1 > num2)
    {
        printf("%d is greater than %d\n", num1, num2);
    }
    
    if(num1 < num2)
    {
        printf("%d is less than %d\n", num1, num2);
    }
    
    return 0;

}
