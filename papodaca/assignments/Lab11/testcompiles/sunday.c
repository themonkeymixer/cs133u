// Author: Patrick
// Title:  Lab 11. Structs & Array functions
// Purpose:funtions galore
// Compile: colorgcc -Wall -pedantic -std=c99 c.c -o c-compile

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>


#define ARRAY_SIZE 	1000
#define STR_LEN 	256


typedef struct {
	char word[STR_LEN];
	int lineNumber;
	int occurance;
}wordInfo;

 //FUNCTIONS
FILE *openFile(void);
void greetings(void);


int buildStruct (FILE *inputFile,wordInfo wordList[ARRAY_SIZE]); 

int print_WordStruct (int x,wordInfo wordList[ARRAY_SIZE]); 		  


//STRUCT


// CALL MAIN() FUNCTION

int main()
{
	FILE *inputFile;
	wordInfo wordList[ARRAY_SIZE];
	int wordsCounted; 
      
	greetings();
	inputFile = openFile();
	printf("file opened\n");
	wordsCounted = buildStruct(inputFile,wordList);
	print_WordStruct(wordsCounted,wordList);

return 0;

}

// GREETINGS DEFINITION

void greetings(void) {
	printf("Functions.c\nBy Patrick\nCredits: Travis\nCompiled as gcc -Wall file.c -o out\n");
} 
// OPENFILE() DEFFINITION	
FILE * openFile() {
     FILE * inputFile;                                      //File pointer
     char inputFileName[STR_LEN] = {0};              //File name
     while (inputFile == NULL)
	 {
        printf("Enter an input file name: ");
        fgets(inputFileName, 256, stdin);
        inputFileName[strlen(inputFileName) - 1] = '\0';
        inputFile = fopen(inputFileName, "r"); 
		if (inputFile == NULL)
        {
            printf("Unable to open \"%s\": %s\n", inputFileName,
                strerror(errno));
        }
		
	}  
    return inputFile;
} 
int buildStruct (FILE *inputFile,wordInfo wordList[ARRAY_SIZE]) {
	int fileLine = 0;
	int i = 0;
	char bufferLine[STR_LEN];
	char delims[] = " ";
	char *result = NULL;
	int wordsCounted = 0;
	
	bool b, *wordFound;

	for(i=0;i<ARRAY_SIZE;i++)	{
		
		wordList[i].lineNumber=0;
		wordList[i].occurance=0;
	}
	b = false;			//initialize 'b' to 'false'
	wordFound = &b;		//point 'wordFound' to first index of 'b'
	
	while(fgets(bufferLine,STR_LEN,inputFile))
	{
		++fileLine;
		result = strtok(bufferLine, delims);
		printf("%s",result);
		getchar();
			while (result!=NULL) 
			{
				for(i=0;i<ARRAY_SIZE;i++)
				{
						strcpy(wordList[i].word, result);
						wordsCounted++;
						wordList[i].occurance++;
						
							
						result = strtok(NULL, delims);
							printf("%s",result);
							getchar();
							*wordFound = false;
							strcpy(wordList[i].word, result);
							wordList[i].lineNumber = fileLine;
							
					}
				
				break;
				}
			continue;	
			}
	}
	fclose(inputFile);
	return wordsCounted;
}

int print_WordStruct(int x,wordInfo wordList[ARRAY_SIZE]) {
  int i = 0;
  printf("The input file contains %d words.\n", x);
    for(i = 0; i < x; ++i) {

      if(wordList[i].lineNumber > 0)

        printf("Word %s first occurs on line %d.\n", wordList[i].word, wordList[i].lineNumber);

    }

    return 0;

}
