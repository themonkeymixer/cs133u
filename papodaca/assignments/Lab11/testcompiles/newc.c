// Author: Patrick
// Title:  Lab 11. Structs & Array functions
// Purpose:funtions galore
// Compile: colorgcc -Wall -pedantic -std=c99 c.c -o c-compile

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#define ARRAY_SIZE 	1000
#define STR_LEN 	256


 //FUNCTIONS
FILE *openFile();
void greetings();

//int buildStruct(FILE* inputFile,struct wordStruct* wordList,int &max_words);

int buildStruct (
        FILE *inputFile,
        struct wordStruct *wordList,
                int max_words; //added ;
); //test changes

//int print_WordStruct(const struct wordStruct *words,int &totalWords);

int print_WordStruct (
        const struct wordStruct *words,
        int totalWords;//added ;
);//test changes

void build_array(
        int array_name,
        int array_length
);


//STRUCT

struct wordStruct{
	char word[STR_LEN];
	int lineNumber;
	int occurance;
};


// CALL MAIN() FUNCTION

int main()
{

	struct wordStruct// { //shouldnt this be here?
	
	wordList[ARRAY_SIZE];

	FILE *inputFile;

	int wordsCounted = 0; 
    //    } //with match here...
        
// FUNCTION CALLS	

	greetings();

	inputFile = openFile();

	buildStruct(inputFile, wordList, wordsCounted);

	print_wordStruct(wordList,wordsCounted);

return 0;

}

// GREETINGS DEFINITION

void greetings(void) {

	printf("Functions.c\nBy Patrick\nCredits: Twenk\nCompiled as gcc -Wall file.c -o out\n");

} 

// OPENFILE() DEFFINITION	

FILE *openFile() {

	FILE *inputfile;

 	char inputFileName[STR_LEN] = {0};

	char *fileName = inputFileName;

        do{

		printf("Enter an input file name: ");

		fgets(inputFileName, STR_LEN, stdin);

		inputFileName[strlen(inputFileName) - 1] = '\0';

		&inputFile = fopen(inputFileName, "r");

		if(inputFile==NULL)

			{

				perror("Failed to open file");

				return EXIT_FAILURE;

			}

        }
		while(inputFile==NULL);

	        return inputFile;

} // FILE * openFile() {


int buildStruct(
        FILE* inputFile, 
        struct wordInfo *wordList {
                wordsCounted; //added ;
                //&wordsCounted test change
        },
)

{
	int fileLine = 0;

	int i = 0;

	char bufferLine[STR_LEN];

	char delims[] = " ";

	char *result==NULL;

	bool b, *wordFound;


	for(i=0;i<ARRAY_SIZE;i++)	{

		wordList[i].lineNumber=0;

		wordList[i].occurance=0;

	}


	b = false;			//initialize 'b' to 'false'

	wordFound = &b;		//point 'wordFound' to first index of 'b'

	
	while(fgets(bufferLine,STR_LEN,inputFile))

	{

		++fileLine;

		bufferLine[strlen(bufferLine)]-1]='\0';

		result = strtok(bufferLine, delims);

			while (result!=NULL) {

				for(i=0;i<wordsCounted;i++) {

					if (strcmp(wordList[i].word, result) == 0) {

						*wordFound = true;

						wordList[i].occurance =+1;

						break;

					}	

				}

				if(*wordFound)	{

				result = strtok(NULL, delims);

				*wordFound = false;

				continue;

				}

				strcpy(wordList[wordsCounted].word, result);

				wordList[wordsCounted].lineNumber = fileLine;

				wordsCounted++;

				result = strtok(NULL, delims);

			}

	}

	fclose(inputFile);

	return 0;

}

int printWordInfo(const struct wordStruct *words, &wordsCounted) {

  

  int i = 0;

  printf("The input file contains %d words.\n", wordsCounted);

    for(i = 0; i < MAX_WORDS; ++i) {

      if(words[i].lineNumer > 0)

        printf("Word %s first occurs on line %d.\n", words[i].word, words[i].lineNumer);

    }

    return 0;

}
