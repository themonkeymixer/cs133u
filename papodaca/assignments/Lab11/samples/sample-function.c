/*
 Doug Jones
 Read a file and count the characters in the file
 Sample program that uses structures.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#define fileNameLength 256

typedef struct {
     char code;                                        //Character
     int  count;                                       //Count of character
     float freq;                                       //Frequency of character
} codeBlock;                                           //structure name

     //Function declarations
int codeBlockComparitor ( const void * elem1, const void * elem2 );
FILE * openFile();                                     //Open input file
codeBlock * countCharacters(FILE *);                   //Count characters


void printCodeBlock(codeBlock *);                      //Print code block
int main(void);                                            //Not really required

   

int main() {
                    //Local variables
     FILE * inputFile;                                 //Input file stream
     codeBlock *freqTable;                             //Frequency table
     
          //Open the file, count the characters, close, and then print
     inputFile = openFile();                           //Open
     freqTable = countCharacters(inputFile);           //Count
     fclose(inputFile);                                //Close the file
     qsort(freqTable, 256, sizeof(codeBlock), codeBlockComparitor); //Sort
     printCodeBlock(freqTable);                        //Print

          //All done
     return 0;
     
}





  //Function definitions

     //Open file for input
FILE * openFile() {
     FILE * inputFile;                                      //File pointer
     char inputFileName[fileNameLength] = {0};              //File name
     
     do {
          printf("Enter input file name: ");                //Prompt
          fgets(inputFileName,fileNameLength,stdin);        //Read the file name
          inputFileName[strlen(inputFileName) - 1] = '\0';  //Kill the \n
          if((inputFile = fopen(inputFileName,"r")) == NULL) {
               printf("Unable to open \"%s\": %s\n",
                      inputFileName,strerror(errno));
          }
          
     } while(inputFile == NULL);                            //Loop until name OK
     
     return inputFile;
}

     //Sort comparitor for codeBlock arrays: Reverse sort (Descending order)
int codeBlockComparitor ( const void * elem1, const void * elem2 ) {
     codeBlock *elemA, *elemB;
     elemA = (codeBlock *)elem1;
     elemB = (codeBlock *)elem2;
     if (elemA->count > elemB->count) {      //Change this to < for normal sort
          return -1;    
     }
     else if(elemA->count == elemB->count) {
          return 0;
     }
     else {
          return 1;
     }
     return 0;
}

     //Function to print a code block
void printCodeBlock(codeBlock * freqTable) {
          //Local variables
     int idx;                                          //Loop variable
     
          //Loop through the code table and print non-zero entries
     for(idx = 0; idx < 256; idx++) {
          if(freqTable[idx].count > 0) {
               printf("Char code %03d ",(int)freqTable[idx].code);  //Header
               
                    //This switch translates unprintable code into their
                    //escape sequence
               switch(freqTable[idx].code) {
                    case '\r':
                         printf("(\\r");
                         break;
                    case '\n':
                         printf("(\\n");
                         break;
                    case '\t':
                         printf("(\\t");
                         break;
                    case '\v':
                         printf("(\\v");
                         break;
                    default:
                         printf(" (%c",freqTable[idx].code);
                         break;
               }
               printf(") occurs %d times\n",freqTable[idx].count);
          }
     }
     
     
     return;
}

     //Function to count characters and return char block array
codeBlock * countCharacters(FILE *inputFile) {
     codeBlock * freqTable;                            //Frequency table
     int idx;                                          //Loop index
     char inByte;                                      //Input byte
     
          //Allocate count table and initialize
     freqTable = calloc(256,sizeof(codeBlock));        //Array of code blocks
     
     for(idx = 0; idx < 256; idx++) {
          freqTable[idx].code = (char)idx;             //Set the ASCII code
          freqTable[idx].count = 0;                    //Zero the count
          freqTable[idx].freq = 0.0;                   //Zero the frequency
     }
          //Count all of the characters
     while((inByte = fgetc(inputFile)) != EOF) {
          freqTable[inByte].count++;                   //Count the byte
     }
     
          //Return the count array
     return freqTable;                                 //Return the array
     
}

