// By Patrick
// Lab 11. Structs & Array functions
// 3/13/2012
// Compile: gcc -std=c99 -Wall file.c -o outputfile

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>


#define ARRAY_SIZE 	1000
#define STR_LEN 	256


 
 
 //FUNCTIONS DECLARATIONS

FILE * openFile();
void greetings(); 
int buildStruct(FILE* inputFile,struct wordList,int wordsCounted);
int print_WordStruct(struct wordList *words,int wordsCounted);
void build_array(int array_name,int array_length);

//STRUCT
struct
wordStruct
{
	char word[STR_LEN];
	int lineNumber;
	int occurance;
	
}wordList;
// CALL MAIN() FUNCTION
int main()
{
	struct wordStruct wordList[ARRAY_SIZE];
	FILE *inputFile;
	int wordsCounted = 0;
	// FUNCTION CALLS	
	greetings();
	inputFile = openFile();
	buildStruct(inputFile, wordList,*wordsCounted);
	print_wordStruct(wordList,*wordsCounted);
return 0;
}


// GREETINGS DEFINITION
void greetings(void) {
	printf("Functions.c\nBy Patrick\nCredits: Twenk\nCompiled as gcc -Wall file.c -o out\n");
} 
// OPENFILE() DEFFINITION	

FILE * openFile() {
     FILE * inputFile;                                      //File pointer
     char inputFileName[STR_LEN] = {0};              //File name
     
     do {
          printf("Enter input file name: ");                //Prompt
          fgets(inputFileName,STR_LEN,stdin);        //Read the file name
          inputFileName[strlen(inputFileName) - 1] = '\0';  //Kill the \n
          if((inputFile = fopen(inputFileName,"r")) == NULL) {
               printf("Unable to open \"%s\": \n",
                      inputFileName);
          }
          
     } while(inputFile == NULL);                            //Loop until name OK
     
     return inputFile;
}

int buildStruct(FILE* inputFile, struct wordStruct *wordList,int)
{
	
	int fileLine = 0;
	int i = 0;
	char bufferLine[STR_LEN];
	char delims[] = " ";
	char *result=NULL;
	bool b, *wordFound;
	
	for(i=0;i<ARRAY_SIZE;i++)	{
		wordList[i].lineNumber=0;
		wordList[i].occurance=0;
	}
	
	b = false;			//initialize 'b' to 'false'
	wordFound = &b;		//point 'wordFound' to first index of 'b'
	
	
	while(fgets(bufferLine,STR_LEN,inputFile))
	{
		++fileLine;
		bufferLine[strlen(bufferLine)-1]='\0';
		result = strtok(bufferLine, delims);
			while (result!=NULL) {
				for(i=0;i<wordsCounted;i++) {
					if (strcmp(wordList[i].word, result) == 0) {
						*wordFound = true;
						wordList[i].occurance =+1;
						break;
					}	
				}
				if(*wordFound)	{
				result = strtok(NULL, delims);
				*wordFound = false;
				continue;
				}
				strcpy(wordList[wordsCounted].word, result);
				wordList[wordsCounted].lineNumber = fileLine;
				wordsCounted++;
				result = strtok(NULL, delims);
			}
	}
	fclose(inputFile);
	return 0;
}

int print_WordStruct(struct wordStruct *wordList,int &wordsCounted) {
  
  int i = 0;
  printf("The input file contains %d words.\n", *wordsCounted);
    for(i = 0; i < ARRAY_SIZE; ++i) {
      if(wordList[i].lineNumber > 0)
        printf("Word %s first occurs on line %d.\n", wordList[i].word, wordList[i].lineNumber);
    }
    return 0;
}
