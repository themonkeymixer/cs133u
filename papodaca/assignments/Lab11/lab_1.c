// By Patrick
// Lab 11. Structs & Array functions
// 3/13/2012
// Compile: gcc -Wall file.c -o outputfile
#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <errno.h>
#define ARRAYSIZE 		1000
#define FILENAMELENGTH 	256

//char fileNameLength[256];
FILE * inputFile;  
char open_file(wordlist)   												// Function to open a file
{

		
		char *inputFileName={0};
        printf("Enter an input file2:");
        scanf("%s",&*inputFileName);
        printf("DEBUG: %s\n",inputFileName);//DEBUG LINE TO MAKE SURE SCANF() WORKS
        inputFile = fopen(inputFileName,"r"); 
        printf("test#aaaaaaaaaaa1\n");  						//DEBUG
        if (inputFile == NULL)
        {
                perror("Failed to open file \"Typed filename\"");
                return EXIT_FAILURE;
        }
		//(int)wordlist=*inputFile;
        return wordlist;
}
/*
int main(int argc, char *argv )
{

        open_file();


}
*/

typedef struct  {
	char word[256];
	int lineNumber;
	int occurance;
} word;

int main(void)
{
//	FILE *inputFile;
	char inputFileName[256] = {0};
	int fileLine=0;
	//int wordlist[10000]={0};

	// wordsCountedSoFar == 0 to begin with
	int wordsCountedSoFar = 0;
	int i;
	char bufferWord[256];
	char *result = NULL;
	char delims[] = " ";
	word Dictionary[1000];
	printf("Program: Structs n Functions\n");
	printf("By: Patrick Apodaca");
    open_file() ;
    
 
 	//while (inputFile == NULL);

		for(i = 0; i < 1000; ++i)
			{

			
			
			
			Dictionary[i].lineNumber = 0; //initialize the line numbers for each word to be 0
										  //that way later on when I've collected all the words	
			Dictionary[i].occurance = 1;  //I will filter the ones with line number not equal to 0

			}

	//read one line at a time from the file
	while (fgets(bufferWord, 256, inputFile))
	{
		//increase the file number
		++fileLine;

		//printf("\nwords found in line %d, %s ",fileLine, bufferWord);
		//remove the linefeed char at the end
		bufferWord[strlen(bufferWord) -1] = '\0';

		//read until the first space (end of word) from line
		result = strtok( bufferWord, delims );

		// keep reading till no more words in the line
		while( result != NULL )
		{


				//store the distinct words
				//wordsCountedSoFar == 0 to begin with
				for(i = 0; i < wordsCountedSoFar; ++i)

				{

					// if the word found in the dictionary, exit from this particular word loop.
					if(strcmp(Dictionary[i].word, result) == 0)

					{

						++Dictionary[i].occurance;
						break;
					}
				}//else iterate through the entire loop

				//if i is equal to wordsCountedSoFar, means word was not found in the dictionary
				//else if i < wordsCountedSoFar, word already exists so don't increase the word count

				strcpy(Dictionary[i].word, result);

				if(wordsCountedSoFar <= i + 1)
				wordsCountedSoFar = i + 1;
				if( Dictionary[i].lineNumber == 0)
					Dictionary[i].lineNumber = fileLine;

					//store the distinct words
					result = strtok( NULL, delims );
		}

	}

	//close file pointer
	fclose(inputFile);

	//PRINT the words in the line they occured first
	for(i = 0; i < 1000; ++i)
	{
		if(Dictionary[i].lineNumber > 0)

			printf("word %s found at line:%d -- total of %d times\n", Dictionary[i].word, Dictionary[i].lineNumber, Dictionary[i].occurance);

	}

	printf("\n\nTotal unique words %d \n\n", wordsCountedSoFar);

    printf("inputFile:\n%s",FILE *inputFile);
	return 0;
}
