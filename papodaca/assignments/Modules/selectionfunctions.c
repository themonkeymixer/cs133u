// By Patrick Apodaca

#include <stdio.h>
#include <string.h>



int getOpType(op){
	switch(op)
	{
		case '|':
		case '&':
		case '!':
			return 1;
		case '<':
		case '>':
			return 2;
	}
	return 0;
}
int compute(a, op, b){
	switch(op)
	{
		case '+':
			return a+b;
		case '-':
			return a-b;
		case '*':
			return a*b;
		case '/':
			return a/b;
		case '%':
			return a%b;
		case '|':
			return a||b;
		case '&':
			return a&&b;
		case '!': //using nand, not is unary...
			return !(a&&b);
	}
}

int main (void)
{
    int a, b, c, d, error=0, result1, result2, result;
    int type1, type2, type3;
    char opA, opB, opC;
    
    printf("Enter an expression: ");
    scanf("(%d %c %d) %c (%d %c %d)", &a, &opA, &b, &opB, &c, &opC, &d);
    
    type1=getOpType(opA);
    type2=getOpType(opB);
    type3=getOpType(opC);
    
    if(type2==0 || type2==2){
    	if(type1+type3==0)
    		error=0;
    	else 
    		error=1;
    }
    else if(type2==1){
    	if(type1>0 && type3>0)
    		error=0;
    	else
    		error=1;
    }
    
    if(!error){
    	result1=compute(a, opA, b);
    	result2=compute(c, opC, d);
    	result=compute(result1, opB, result2);
    	printf("Result:(%d %c %d) %c (%d %c %d) = %d", a, opA, b, opB, c, opC, d, result);
    }
    else{
    	printf("Result:(%d %c %d) %c (%d %c %d) is not  a valid expression for this program\n",\
    		a, opA, b, opB, c, opC, d);
    }
    return 0;
}