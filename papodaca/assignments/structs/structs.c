// Author: Patrick Apodaca
// Title: Structs
// Purpose: To read input file then see how many times a word reoccurs on that same line.
// Credit: Travis Wenks

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define fileNameLength 256

typedef struct  {
	char word[256];
	int lineNumber;
	int occurance;
} word;

int main()
{
	FILE *inputFile;
	char inputFileName[256] = {0};
	int fileLine = 0;

	// wordsCountedSoFar == 0 to begin with
	int wordsCountedSoFar = 0;
	int i;
	char bufferWord[256];
	char *result = NULL;
	char delims[] = " ";
	word Dictionary[1000];
	printf("Program: Structs\n");

	do
	{
		//opening the file... same as array lab
		printf("Enter an input file name: ");
		fgets(inputFileName, fileNameLength, stdin);
		inputFileName[strlen(inputFileName) - 1] = '\0';
		if ((inputFile = fopen(inputFileName, "r")) == NULL)
		{
			printf("Unable to open \"%s\": %s\n", inputFileName,
				strerror(errno));
		}
	}
	
	while (inputFile == NULL);

		for(i = 0; i < 1000; ++i)
			{
		/* 
		 * initialize the line numbers for each word to be 0
		 * that way later on when I've collected all the words
		 * I will filter the ones with line number not equal to 0
		 */
			Dictionary[i].lineNumber = 0;
			Dictionary[i].occurance = 1;

	}

	//read one line at a time from the file
	while (fgets(bufferWord, 256, inputFile))
	{
		//increase the file number
		++fileLine;

		//printf("\nwords found in line %d, %s ",fileLine, bufferWord);
		//remove the linefeed char at the end
		bufferWord[strlen(bufferWord) -1] = '\0';

		//read until the first space (end of word) from line
		result = strtok( bufferWord, delims );

	// keep reading till no more words in the line
	while( result != NULL )
	{


			//store the distinct words
			//wordsCountedSoFar == 0 to begin with
			for(i = 0; i < wordsCountedSoFar; ++i)

			{

				// if the word found in the dictionary, exit from this particular word loop.
				if(strcmp(Dictionary[i].word, result) == 0)

				{

					++Dictionary[i].occurance;
					break;
				}
			}//else iterate through the entire loop

			//
			strcpy(Dictionary[i].word, result);			/* if i is equal to wordsCountedSoFar, means word
														   was not found in the dictionary,
														   else if i < wordsCountedSoFar, word already exists
														   so don't increase the word count
														   */
			if(wordsCountedSoFar <= i + 1)
			wordsCountedSoFar = i + 1;
			if( Dictionary[i].lineNumber == 0)
				Dictionary[i].lineNumber = fileLine;

			// store the distinct words
			result = strtok( NULL, delims );
		}

	}

	//close file pointer
	fclose(inputFile);

	//PRINT the words in the line they occured first
	for(i = 0; i < 1000; ++i)
	{
		if(Dictionary[i].occurance > 1)

			printf("word %s found at line:%d -- total of %d times\n", Dictionary[i].word, Dictionary[i].lineNumber, Dictionary[i].occurance);

	}

	printf("\n\nTotal unique words %d \n\n", wordsCountedSoFar);

	return 0;

}