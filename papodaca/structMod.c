/*
Author: Dan Fellin
Class: CS133
Assignment: Structs 
Due Date: 3/11/2012
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 256
#define MAX_WORDS 1000

//FUNCTION PROTOTYPES
FILE* openFile();
int readFileIntoStruct(FILE* inputFile, struct wordInfo* wordList, int totalWords);
int printWordInfo(const struct wordInfo *words, int totalWords);

//CUSTOM STRUCT 
struct wordInfo {  
  char word[STR_LEN];
  int lineNum;       
};            

//BEGIN MAIN
int main() {

  struct wordInfo wordList[MAX_WORDS];
  FILE *inputFile;
  int totalWords = 0;

  inputFile = openFile();
  readFileIntoStruct(inputFile, wordList, totalWords);
  printWordInfo(wordList, totalWords);

return 0;
}


FILE* openFile() {

  FILE *inputFile;
  char inputFileName[STR_LEN] = {0};

  do { 
    printf("Enter an input file name: ");
    fgets(inputFileName, STR_LEN, stdin);
    inputFileName[strlen(inputFileName) - 1] = '\0';
    if ((inputFile = fopen(inputFileName, "r")) == NULL)
      printf("Unable to open %s\n", inputFileName); 
  } while (inputFile == NULL);
  
  return inputFile;
}

int readFileIntoStruct(FILE* inputFile, struct wordInfo *wordList, &totalWords) {

  int fileLine = 0;
  int i = 0;
  char lineIn[STR_LEN];
  char delims[] = " ";
  char *result;
  bool b, *wordFound;

  for(i = 0; i < MAX_WORDS; ++i) {
    wordList[i].lineNum = 0;
  }

  b = false; //initialize bool "b" to false
  wordFound = &b; //point wordFound to the address of b

  while (fgets(lineIn, STR_LEN, inputFile)) {
    ++fileLine;
    lineIn[strlen(lineIn) - 1] = '\0';
    result = strtok(lineIn, delims);

    while( result != NULL) { //

      for(i = 0;i < totalWords; i++) {
        if (strcmp(wordList[i].word, result) == 0) {
          *wordFound = true;
          break;
        }
      }

      if (*wordFound) {
        result = strtok(NULL, delims);
        *wordFound = false;
        continue; //Go back to top of while (result != NULL) loop
      }

      strcpy(wordList[totalWords].word, result); //If the word wasn't found, then copy it over.
      wordList[totalWords].lineNum = fileLine; //count the word
      totalWords++;
      result = strtok(NULL, delims);
    }
  }
  fclose(inputFile);
  return 0;
}

int printWordInfo(const struct wordInfo *words, &totalWords) {
  
  int i = 0;
  printf("The input file contains %d words.\n", totalWords);
    for(i = 0; i < MAX_WORDS; ++i) {
      if(words[i].lineNum > 0)
        printf("Word %s first occurs on line %d.\n", words[i].word, words[i].lineNum);
    }
    return 0;
}
