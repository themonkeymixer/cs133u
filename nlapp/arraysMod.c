/*******************************************************************/
/* AUTHOR: Nick Lapp************************************************/
/* PROGRAM: arraysMod.c ********************************************/
/* DATE: Sat, Mar 17th *********************************************/
/*******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <strings.h>
#define MAX_LINE 254

typedef struct
{
       	FILE * inputFile;
      	char fileName[MAX_LINE]; 
} fileHolder;
	
typedef struct 
{
	char character; int count;
	float frequency;
} charCount;

fileHolder * openFile();
charCount * countCharacters(FILE * inputFile);
charCount * printChars(charCount * myCountedArray);
void printEntropy(charCount * myCountedArray);
unsigned short int getCeasarShift();
FILE * openOut();
void writeToOut(FILE * outputFile, char * fileName, unsigned short int ceasarShift);

int main() {
	fileHolder * inputFile;
	FILE * outputFile;
	charCount * myCountedArray;
	unsigned short int ceasarShift;

	inputFile = openFile();
	ceasarShift = getCeasarShift();
	myCountedArray = countCharacters(inputFile->inputFile);
	myCountedArray = printChars(myCountedArray);
	printEntropy(myCountedArray);
	outputFile = openOut();
	writeToOut(outputFile, inputFile->fileName, ceasarShift);	
	
	return 0;
}

fileHolder * openFile()
{
	fileHolder * inputFileHolder;
	inputFileHolder = calloc(1, sizeof(fileHolder));
 	
	printf("Program: arraysMod.c\n");
	printf("Enter text file from which to be read: ");
        scanf("%s", inputFileHolder->fileName);

	for(;;)
        {
                inputFileHolder->inputFile = fopen(inputFileHolder->fileName, "r");
                if(inputFileHolder->inputFile == NULL)
                { 
                        printf("Error: Unable to open file.\n");
                        printf("Please insert a valid filename\n");
                        printf("Enter text file from which to be read: ");
                        scanf("%s", inputFileHolder->fileName);
                }
                else
                {
                        return inputFileHolder;       
                }
        }
}

charCount * countCharacters(FILE * inputFile)
{
	int loopCounter;
	char inBetweenChar;
	charCount * myCountedArray = calloc(257, sizeof(charCount));
	
	for(loopCounter = 0; loopCounter <= 256; loopCounter++)
	{
		myCountedArray[loopCounter].character = (char)loopCounter;
		myCountedArray[loopCounter].count = 0;
		myCountedArray[loopCounter].frequency = 0.0f;
	}
	printf("The text read was:\"");	
	while((inBetweenChar = getc(inputFile)) != EOF)
	{
		printf("%c", inBetweenChar);	
		myCountedArray[inBetweenChar].count++;
	}
	printf("\"\n");
	fclose(inputFile);
	return myCountedArray;
}

unsigned short int getCeasarShift()
{
	unsigned short int shift;
        printf("Enter an unsigned short integer for the ceasar shift: ");
       	for(;;)
	{
		if (scanf("%hu", &shift) == 1)
		{
			return shift;
		}
		else
		{
			printf("\nUnable to read short int. Please insert another: ");
		}
	} 
}

charCount * printChars(charCount * myCountedArray)
{
	int loopCounter = 0;
	int totalCount = 0;

	for(loopCounter = 0; loopCounter<256; loopCounter++)
	{
		totalCount += myCountedArray[loopCounter].count;
	}
	printf("There were %d characters printed in this file\n", totalCount);	
	printf("The frequencies of this file are: \n");
	for(loopCounter = 0; loopCounter<256; loopCounter++)
	{
		if(myCountedArray[loopCounter].count > 0)
		{
		
		switch(myCountedArray[loopCounter].character) {
                    case '\r':
                         printf("\"\\r\"");
                         break;
                    case '\n':
                         printf("\"\\n\"");
                         break; case '\t':
                         printf("\"\\t\"");
                         break;
                    case '\v':
                         printf("\"\\v\"");
                         break;
                    default:
                         printf("\"%c\"",myCountedArray[loopCounter].character);
                         break;
                }
		printf(" has an absolute frequency of: %d\t", myCountedArray[loopCounter].count);
		printf("and a relative frequency of: %f\n", (myCountedArray[loopCounter].frequency = (double)myCountedArray[loopCounter].count / (double)totalCount));
		}
	}
	return myCountedArray;
}
	
void printEntropy(charCount * myCountedArray)
{
	int loopCounter = 0;
	int totalCount = 0;
	float perCharEntropy = 0;	

	for(loopCounter = 0; loopCounter<256; loopCounter++)
	{
		totalCount += myCountedArray[loopCounter].count;
		if(myCountedArray[loopCounter].frequency != 0)
		{	
			perCharEntropy += log2(myCountedArray[loopCounter].frequency) * myCountedArray[loopCounter].frequency;
		}
	}	
	perCharEntropy *= -1;
	printf("The per-character Entropy is %f\n", perCharEntropy);
	printf("The total Entropy is %f.\n", perCharEntropy * (float)totalCount);
	return;
}	
		
FILE * openOut()
{
	FILE * outputFile = 0; char fileName[MAX_LINE] = "out.txt"; 
	for(;;)
        {
                outputFile = fopen(fileName, "w");
                if(outputFile == NULL)
                { 
                        printf("Error: Unable to open file.\n");
                        printf("Please insert a valid filename\n");
                        printf("Enter text file from which to be read: ");
                        scanf("%s", fileName);
                }
                else
                {
                        return outputFile;       
                }
        }
}

void writeToOut(FILE * outputFile, char * fileName, unsigned short int ceasarShift)
{
	char inBetweenChar;	
	unsigned short int shift = ceasarShift;	
	FILE * inputFile = 0;
	if((inputFile = fopen(fileName, "r")) == NULL)
	{
		printf("Unable to reopen %s. Error", fileName);
		return; 
	}
	while((inBetweenChar = fgetc(inputFile)) != EOF)
        {
                if(inBetweenChar >= 'A' && inBetweenChar <= 'Z')
                {
                        inBetweenChar += shift;
                        while(inBetweenChar > 'Z')
                        {
                                inBetweenChar -= 26;
                        }
                }

                else if(inBetweenChar >= 'a' && inBetweenChar <= 'z')
                {
                        inBetweenChar += shift;
                        while(inBetweenChar > 'z')
                        {
                                inBetweenChar -= 26;
                        }
                }
                fprintf(outputFile, "%c", inBetweenChar);
        }       
	        
        printf("The file out.txt has been written with a ceasar shift of %hu.\n", shift);
	fclose(inputFile);
	fclose(outputFile);
	return;
} 
