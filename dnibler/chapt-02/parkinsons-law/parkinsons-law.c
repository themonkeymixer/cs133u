// Name: Parkinson's Law
// Purpose: Print Parkinson's Law
// Author:  Travis Wenks

#include <stdio.h>

int main(void)
{

  printf("Parkinson's Law: \nWork expands so as to ");
  printf("fill the time\n");
  printf("available for completion\n");

  return 0;
}

