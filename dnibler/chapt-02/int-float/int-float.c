// Title: Int - Float test 
// Purpose: Show the affects of printing uninitialized variables
// Author: Travis Wenks


#include <stdio.h>

int main (void)
{

  int height, length, width, volume, weight;

  printf("Dimensions: %dx%dx%d\n", length, width, height);
  printf("Volume (cubic inches): %d\n", volume);
  printf("Dimensional weight (pounds): %f\n", weight);

  return 0;
}


