
/*
doodles, ignore the no-adherence to style-mashtyle
strings
*/
/*
read a bit about arrays
arrays let you store more than one value with the same variable name
for example, if I had to store values for 100 students, what would be a better option
1. Create 100 variables
2. Create 1 variable which is a list of 100 (Pick me !!! Pick me!!!)

Arrays are a means to achieve the latter.
<type> nameOfArray[SIZE in int]
You start counting the slots/ index of an array from 0. If an array is of size 10, it'll've
array[0], array[1],...,array[9]

int myIntArray[25]; creates a variable named myIntArray which can store 25 different INTEGER values
float myFloatArray[100];creates a variable named myFloatArray which can store 100 different DECIMAL values

char myCharArray[50]; creates a variable named myCharArray which can store 49 different char values
.
.
.
WAIT... WHAT?
Yes char arrays or strings(list of characters associated with one variable name, for example myCharArray above)
have special quirks
1. They need one space for the string delimiter(a flag for compiler to know that the string has ended)
so a string/ char array myCharArray can legally store only 49 characters max
2. You can read a string using scanf("%s", myCharArray) but omit the & in fromt of the string variable name
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char SingleChar;
    char mySentence[30] = "My name is Kavita"; /*can initialize a string when declared*/
    char yetAnotherSentence[3] = "OR";
    int intArr[30]; /* for perspective, will dwell on arrays later in class */
    int lenOfMySent;

    SingleChar = 'K';

    printf("The text in mySentence is [%s]\n", mySentence);

    mySentence[11] = 'S'; /* pick one slot in mySentence which is an array and modify */
    printf("The text in mySentence after updating one alphabet is [%s]\n", mySentence);
    printf("printing mySentence from 7'th slot onwards [%s]\n", mySentence + 7);

    printf("scanf is a fragile way of reading a string \n \
           1. One it doesn't read a sentence, will stop at the first space\n \
           2. Will not check whether you've crossed the limit/ size for the string variable\n \
           ");
    printf("\nEnter a word(no space and <=29 in size)\n\tto read into mySentence using scanf :");
    scanf("%s", mySentence); /*Look Ma, no & (in front of mySentence)*/
    printf("The text in mySentence is with value from user is [%s]\n", mySentence);
    scanf("%*c"); /*gobble up all extra space to get ready to read the next string*/
    printf("\nEnter a sentence(<=29 in size)\n to read into mySentence using fgets :");
    fgets(mySentence,29,stdin);
    printf("The text in mySentence is with value from user with the newline is \n[%s]\n", mySentence);

    lenOfMySent = strlen(mySentence); /* using the strlen in string.h library to get the length of the mySentence
                                      includes the newline that the user entered to indicate done entering the string
                                      but does not include the string delimitor
                                      */

    mySentence[lenOfMySent - 1] = '\0'; /*manually get rid of the newline that the user entered to indicate done entering the string*/
    printf("The text in mySentence now that we've taken away the end newline is\n [%s]\n", mySentence);
    printf("The length of mySentence now that we've taken away the end newline is [%d]\n", lenOfMySent);
    printf("The sizeof of mySentence (the array) is [%d]\n", sizeof(mySentence));
    printf("The sizeof of intArr (the array) is [%d]\n", sizeof(intArr));

    system("pause");
    return 0;  
}
