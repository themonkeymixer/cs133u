
/*
doodles, ignore the no-adherence to style-mashtyle
strings
some more
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char mySentence[50]; /*can initialize a string when declared*/
    char yetAnotherSentence[50];
    char concatedSentence[50];

    int lenOfMySent = 0, lenOfYetASent = 0, lenOfConcatSent = 0;

    printf("///////////////////////////////////////////////////////////////////\n");
    printf("\nEnter a sentence(<=49 in size)\n to read into mySentence using fgets :");
    fgets(mySentence,49,stdin);
    printf("The value in mySentence is %s", mySentence);
    printf("The text in mySentence with value from user with the newline is \n[%s]\n", mySentence);

    lenOfMySent = strlen(mySentence); /* using the strlen in string.h library to get the length of the mySentence
                                      includes the newline that the user entered to indicate done entering the string
                                      but does not include the string delimitor
                                      */

    mySentence[lenOfMySent - 1] = '\0'; /*manually get rid of the newline that the user entered to indicate done entering the string*/
    printf("The text in mySentence now that we've taken away the end newline is\n [%s]\n", mySentence);
    printf("The length of mySentence now that we've taken away the end newline is [%d]\n", lenOfMySent);
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    printf("///////////////////////////////////////////////////////////////////\n");
    printf("\nEnter a sentence(<=49 in size)\n to read into yetAnotherSentence using fgets :");
    fgets(yetAnotherSentence,49,stdin);
    printf("The value in yetAnotherSentence is %s", yetAnotherSentence);
    printf("The text in mySentence with value from user with the newline is \n[%s]\n", yetAnotherSentence);

    lenOfYetASent = strlen(yetAnotherSentence); /* using the strlen in string.h library to get the length of the mySentence
                                                includes the newline that the user entered to indicate done entering the string
                                                but does not include the string delimitor
                                                */

    yetAnotherSentence[lenOfYetASent - 1] = '\0'; /*manually get rid of the newline that the user entered to indicate done entering the string*/
    printf("The text in yetAnotherSentence now that we've taken away the end newline is\n [%s]\n", yetAnotherSentence);
    printf("The length of yetAnotherSentence now that we've taken away the end newline is [%d]\n", lenOfYetASent);
    printf("///////////////////////////////////////////////////////////////////\n");
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*************************** Copy first 1/2 of mySentence onto concatedSentence  ****************************************/
    strncpy(concatedSentence, mySentence, lenOfMySent / 2); /* read on strncpy*/
    /* lenOfMySent / 2  = 1/2 the lemgth of the first string mySentence */
    /*
    Now concatedSentence has the first 1/2 of the first string mySentence
    BUT
    is not terminated by the string terminating character '\0' which tells the
    compiler that a string has ended
    so is not a legit string as yet
    */
    concatedSentence[lenOfMySent / 2] = '\0'; /* append the null ('\0') at the end of the string to make it legal*/
    printf("The text in concatedSentence with the first 1/2 of first string is [%s]\n", concatedSentence);
    /*************************** Copy second 1/2 of yetAnotherSentence onto concatedSentence  ****************************************/

    system("pause");
    return 0;  
}
