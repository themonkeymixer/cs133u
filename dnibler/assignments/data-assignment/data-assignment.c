// Author: Daniel Nibler
// Title: Data Assignment
// Credits: Original Source, Nautiyal, Kavita, Travis Wenks
// Purpose: declare and print variables

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>  

int main(void)
{
    char string[] = "Hello World! I am a string!";
    // declare string = statement

    char character = 'a';
    // declare character with value "a"

    int shortinteger = SHRT_MAX;
    // declare variable for max short value

    double dbl = INFINITY;
    // declare viariable for infinity value
    
    int *p;

    p = &shortinteger; 
    // declare pointer to max short value


    printf("display string: %s\n", string);

    printf("display character: %c\n", character);

    printf("display character in decimal: %d\n", character);
   
    printf("display max short value of type: %d\n", shortinteger);

    printf("dsplay double variable: %f\n", dbl);

    printf("display pointer to max short value: %d\n", *p);

    printf("display value of short int: %d\n", p);

    return 0;
}
