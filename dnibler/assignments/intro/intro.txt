
What is your name and G-Number? G02781335
Why are you taking CS 133U? I'm going to school for an engineering degree, i know i will have to do computer programming eventually, so i decided to take this one.
What type of programming experience do you have? absolutely none.
What type of system (Windows, Linux, Apple, etc.) will you use to develop programs? linux
What IDE (DevC++, Visual Studio, Xcode, etc.) will you use to develop programs? vim
Based on what you know at this time, what do you dislike most about this course? uncertainty. i've gotten through the first two chapters of the text book, and i'm still not sure if much of it has stuck.
Based on what you know at this time, what do you like most about this course? i enjoy learning new languages, and this is just another one to learn, i think i will enjoy it.
What are the most important things the instructor should know about you? i'm really not sure. sorry.	
What are the most important things the instructor should do to help you? as long as i have a syllabus and can contact you when necessary i should be ok. 	
If an in-person meeting is needed with your instructor, which PCC campus would be most convenient? newberg is where i live, however i'm at sylvania tuesday thursday and friday. so which ever.

