// Author: Daniel Nibler 
// Contributors: Travis Wenks 
// Title: hello world
// Purpose: to demonstrate the printf command using hello world


#include <stdio.h> 
#include <stdlib.h> 

int main(void)
    {

    printf("hello world!\n");
    
    return 0;
    
    } 
