// Author: Daniel Nibler        
// Title: stringProcessing.c
// Purpose: do string shtuff
// Credit: Travis Wenks, Kavita Nautiyal
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
    
int main(void) 
{

    char strng1[254], strng2[254], strng3[254];
    int strlen1, strlen2;

    printf("Enter string 1: ");
    fgets(strng1, 254, stdin);

    printf("Enter string 2: ");
    fgets(strng2, 254, stdin);

    strlen1 = strlen(strng1);
    strlen2 = strlen(strng2);
  
    printf("String 1 is %d bytes long, and String 2 is %d bytes long \n", strlen1, strlen2);
    
    strncpy(strng3, strng1, strlen1 / 2);
    strng3[strlen1 / 2] = '\0';
    strcat(strng3, strng2 + strlen2 / 2);

    printf("String 3 is: %s", strng3);

    return 0;
}

