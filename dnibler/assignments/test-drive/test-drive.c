// Author: Daniel Nibler
// Title: test drive
// Purpose: create test drive program
   
   
#include <stdio.h>
#include <stdlib.h>

int main(void)
        { 
        
        printf("1. G02781335\n");
        printf("2. I'm going to school for an engineering degree, i know i will have to do\n");
        printf("computer programming eventually, so i decided to take this one.\n");
        printf("3. absolutely none\n");
        printf("4. linux\n");
        printf("5. vim\n");
        printf("6. uncertainty. i got through the first two chapters of the text book,\n");
        printf("and i'm still not sure if much of it has stuck.\n");
        printf("7. i enjoy learning new languages, and this is just another one to learn,\n");
        printf("i think i will enjoy it.\n");
        printf("8. i'm really not sure. sorry.\n");
        printf("9. as long as i have a syllabus and can contact you when\n");
        printf("necessary i should be ok.\n");
        printf("10. sylvania\n");

        return 0;

        }



