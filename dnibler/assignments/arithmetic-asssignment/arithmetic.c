// Author: Daniel Nibler
// Title: Arithmetic Expressions
// Credits: Travis Wenks
// Purpose: display specific mathematical expressions

#include <stdio.h>
#include <stdlib.h>
#include <math.h>  

int main(void)
{
   
  unsigned short Shrt, Shrt2, Shrtr, cir, area, pi;
    
    float flt1, flt2, flt3, fltS;

    printf("enter unsigned short integer:");
    
    scanf("%hu", &Shrt); 

    Shrt2 = pow(2, Shrt);

    printf("the value of 2 raised to the power");
    
    printf("of %hu is = %hu\n", Shrt,Shrt2);
    
    printf("enter unsigned short integer:");

    pi = 3.14159 ;
   
    scanf("%hu", &Shrtr);

    cir = 2*pi*Shrtr;

    area = pi*Shrtr*Shrtr;

    printf("A circle with radius %hu  has circumference\n", Shrtr);
    
    printf("of %hu and an area of %hu\n", cir, area);

    printf("Enter a float value: ");

    scanf("%f", &flt1);

    flt2 = 2*flt1*flt1*flt1;

    flt3 = 3*flt1*flt1;

    fltS = 4*flt2 + flt2 + flt3 +5;

    printf("the value of 2(%5.2f)**3 + 3*(%5.2f)**2", flt1, flt1);
   
    printf("+ 4(%5.2f) + 5 = %5.2f\n", flt1, fltS);

    return 0;
}
