/*
Author: Dan Fellin
Title: Arithmetic Expressions
Class: CS133u
Due Date: 2/28/12
Description: Creating, manipulating, and displaying arithmetic expressions
*/

#include <stdio.h>
#include <stdbool.h>

int arithcalc(int num1, char op, int num2);
bool condcalc(int num1, char op, int num2);
bool logicalc(int num1, char op, int num2);

int main() {
  int i, j;
  int a, b, c, d;
  int tmp1, tmp2, result;
  bool test1, test2, test3;
  char oper[3];
  //The user-supplied operators are stored in this array.
  char opType[3] = {'f', 'f', 'f'};
  //The type of operator for the corresponding oper[i] is stored in this array.
  const int arithmetic[5] = {'+', '-', '*', '/', '%'};
 //-999 is a placeholder so we can use the same for loop to step through all three arrays.
  const int condition[5] = {'<', '=', '>', -999, -999};
  const int logic[5] = {'&', '|', '!', -999, -999};

  printf("Please enter values for the following expression: (a x b) y (c z d)\n"
         "Where a, b, c, and d are integer values; x, y, z are single characters representing operators.\n"
         "The parentheses and spaces must be entered as shown above: ");
  scanf("(%d %c %d) %c (%d %c %d)", &a, &oper[0], &b, &oper[1], &c, &oper[2], &d);

  //Determine the type of each operator and store it in opType[j]
  for(j = 0; j < 3; j++) { 
    for(i = 0; i < 5; i++) {
      if (oper[j] == arithmetic[i]) 
        opType[j] = 'a';
      else if (oper[j] == condition[i]) 
        opType[j] = 'c';
      else if (oper[j] == logic[i]) 
        opType[j] = 'l';
      }
    }
  
  //opType defaulted to 'f' originally. Check to make sure all those default f's have been overwritten with a valid type.
  for(i = 0; i < 3; i++)
    if (opType[i] == 'f') {
      printf("Invalid value or operator entered. Program terminating.\n");
      return -1;
   }

  if(opType[0] == opType[1] && opType[1] == opType[2])
    printf("All your operators have the same type! Nice work!\n");
  else { 
    printf("All your operators need to have the same type. Program terminating. Please try again.\n");
    return -1;
  }

  printf("(%d %c %d) %c (%d %c %d) = ", a, oper[0], b, oper[1], c, oper[2], d);

  switch (opType[0]) {
    case 'a': 
      tmp1 = arithcalc(a, oper[0], b);
      tmp2 = arithcalc(c, oper[2], d);
      result = arithcalc(tmp1, oper[1], tmp2);
      printf("%d\n", result);
      break;
    case 'c':
      test1 = condcalc(a, oper[0], b);
      test2 = condcalc(c, oper[2], d);
      test3 = condcalc(test1, oper[1], test2);
      if (test3) printf("true\n");
      else printf("false\n");
      break;
    case 'l':
      test1 = logicalc(a, oper[0], b);
      test2 = logicalc(c, oper[2], d);
      test3 = logicalc(test1, oper[1], test2);
      if (test3) printf("true\n");
      else printf("false\n");
      break;
    }
  return 0;
}

int arithcalc(int num1, char op, int num2) {
  switch (op) {
    case '+': return (num1 + num2);
    case '*': return (num1 * num2);
    case '-': return (num1 - num2);
    case '/': return (num1 / num2);
    case '%': return (num1 % num2);
  }
}

bool condcalc(int num1, char op, int num2) {
  switch (op) {
    case '<': return num1 < num2;
    case '=': return num1 == num2;
    case '>': return num1 > num2;
  }
}

bool logicalc(int num1, char op, int num2) {
  switch (op) {
    case '&': return num1 && num2;
    case '|': return num1 || num2;
    case '!': return num1 != num2;
  }
}
