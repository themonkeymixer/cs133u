/*
Author: Dan Fellin
Title: Arithmetic Expressions
Class: CS133u
Due Date: 2/28/12
Description: Creating, manipulating, and displaying arithmetic expressions
*/

#include <stdio.h>
#include <math.h>
#define PI 3.14159265348979323846


int main() {
  unsigned short i;
  unsigned short r;
  float fl;

  printf("Please enter a positive integer less than 65,535: ");
  scanf("%hu", &i);
  printf("The value of 2 raised to the %hu power is %.f\n", i, pow(2,i));
  printf("Please enter the radius of the circle: ");
  scanf("%hu", &r);
  printf("The circumference of the circle is %.3f\n", 2 * PI * r); 
  printf("The area of the circle is %.3f\n", PI * pow(r,2)); 
  printf("For the polynomial 2x^3 + 3x^2 + 4x + 5: please enter a value for x: ");
  scanf("%f", &fl);
  printf("The expression evaluates to %.3f\n", (2 * pow(fl,3) + 3 * pow(fl,2) + 4 * fl + 5));
  return 0;
}
