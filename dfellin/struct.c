/*
Author: Dan Fellin
Class: CS133
Assignment: Structs 
Due Date: 3/11/2012
 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STR_LEN 256

  struct wordInfo {  
    char word[STR_LEN];
    int lineNum;       
  };            

int main() {
  struct wordInfo wordList[1000];

  FILE *inputFile;
  char inputFileName[STR_LEN] = {0};

  int fileLine = 0;
  int totalWords = 0;
  int i;

  char lineIn[STR_LEN];
  char delims[] = " ";
  char *result;
  bool b, *wordFound;

  do { 
    printf("Enter an input file name: ");
    fgets(inputFileName, STR_LEN, stdin);
    inputFileName[strlen(inputFileName) - 1] = '\0';
    if ((inputFile = fopen(inputFileName, "r")) == NULL)
      printf("Unable to open %s\n", inputFileName); 
  } while (inputFile == NULL);

  for(i = 0; i < 1000; ++i) {
    wordList[i].lineNum = 0;
  }

/*
It's cool how the hint2 program checks whether or not the word is already in the list.
Specifically, it checks to see how far the for loop got before it terminated. And then uses that
to figure out whether or not the word already exists.

I can't decide if my algorithm is more efficient or not.
The big inefficiency in Hint2 is that it copies each and every word read into
"result" somewhere into the array, oftentimes copying over the same word.

Mine doesn't. However, mine does add two boolean variables not needed by Hint2's algorithm.
And it requires more lines of code, though I don't think more comparisons on average...

Mine has more boolean assignments, but that has to be way more efficient than copying strings.

So Hint2 requires less memory, but only 2 boolean's (2 bits) worth. I think the processor gains of my
algorithm are more than worth that...

Also, I feel like my code might be easier to grok. Flipping the bit on a wordFound variable feels more
natural to a human mind than trying to think in terms of where the index ended up.

*/

  b = false; //initialize bool "b" to false
  wordFound = &b; //point wordFound to the address of b

  while (fgets(lineIn, STR_LEN, inputFile)) {
    ++fileLine;
    lineIn[strlen(lineIn) - 1] = '\0';
    result = strtok(lineIn, delims);

    while( result != NULL) { //

      for(i = 0;i < totalWords; i++) {
        if (strcmp(wordList[i].word, result) == 0) {
          *wordFound = true;
          break;
        }
      }

      if (*wordFound) {
        result = strtok(NULL, delims);
        *wordFound = false;
        continue; //Go back to top of while (result != NULL) loop
      }

      strcpy(wordList[totalWords].word, result); //If the word wasn't found, then copy it over.
      wordList[totalWords].lineNum = fileLine; //count the word
      totalWords++;
      result = strtok(NULL, delims);
    }
  }

  fclose(inputFile);
  printf("The input file contains %d words.\n", totalWords);
  for(i = 0; i < 1000; ++i) {
    if(wordList[i].lineNum > 0)
      printf("Word %s first occurs on line %d.\n", wordList[i].word, wordList[i].lineNum);
  }

return 0;
}

