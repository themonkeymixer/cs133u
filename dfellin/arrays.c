/*
Author: Dan Fellin
Title: Arrays
Class: CS133u
Due Date: 3/4/12
Description: Arrays
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAX_LEN 256

int main() {
  int i;

  char file[MAX_LEN];
  FILE *inf = 0; //input file
  FILE *outf = 0; //output file, encrypted by Caesar cipher

  char inChar = '\0';
  char encryptChar = '\0';

  unsigned short cipher; //Caesar cipher
  int freqChar[256] = {0}; //store each char in inf by ascii value here
  int totalChars = 0;

  float frequency = 0;
  float entropy = 0;


	printf("Program : arrays.c\n");
	printf("Author  : Dan Fellin\n\n");

  printf("Please input the name of the file you would like to analyze: ");
  fgets(file,MAX_LEN,stdin);        //Read the file name
  file[strlen(file) - 1] = '\0';  //Kill the \n

  inf = fopen(file, "r");
  if (inf == NULL) {
    printf("Error. Could not open file.\n");
    return -1;
  }
  
  printf("Please enter an unsigned short value for the Caesar cipher: ");
  scanf("%hu", &cipher);


  while((inChar = fgetc(inf)) != EOF) {
    ++freqChar[inChar];                   //Count the char 
    ++totalChars;                         // total characters read so far
  }
  fclose(inf);                              

  printf("%d characters were read.\n"
         "The frequencies for file '%s' are:\n\n", totalChars, file);



  for(i = 0; i < 256; i++) {
    if (freqChar[i] > 0) {
			frequency = (float) freqChar[i] / (float) totalChars;
			entropy += (float) (frequency * (log(frequency) / log(2)));

      printf("Char code %03d ",i); /* no new line, printing the ascii value of the character */
      switch((char)i) { /*convert i to char, which'll give me the literal value of the character*/
        case '\r':
          printf("(\\r"); /*special*/
          break;
        case '\n':
          printf("(\\n");
          break;
        case '\t':
          printf("(\\t");
          break;
        case '\v':
          printf("(\\v");
          break;
        default:
          printf(" (%c",i);
          break;
            }
		   	printf(") occurs %d times for a relative frequency of %.3f\n", freqChar[i], frequency);
    }
  }
 
	entropy *= -1;
	printf("\nThe per character entropy is : %.3f\n", entropy);
	printf("The total entropy is         : %.3f\n", (entropy * totalChars));

  inf = fopen(file, "r");
  if (inf == NULL) {
    printf("Error. Could not open file.\n");
    return -1;
  }

  outf = fopen("out.txt", "w+"); //I decided to just overwrite any previous 'out.txt' file here.

  while((inChar = fgetc(inf)) != EOF) {
    if(inChar >= 'A' && inChar <= 'Z') {
      //Make any (char + cipher) > Z start counting back at A
      encryptChar = ((inChar - 'A') + cipher) % 26 + 'A'; 
      fprintf(outf,"%c", encryptChar);
    }
    else if (inChar >= 'a' && inChar <= 'z') {
      //Make any (char + cipher) > z start counting back at a
      encryptChar = ((inChar - 'a') + cipher) % 26 + 'a'; 
      fprintf(outf,"%c", encryptChar);
    }
    else fprintf(outf,"%c", inChar);
  }
  fclose(inf);
  fclose(outf);

  return 0;
}

