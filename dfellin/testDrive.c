/*
    Author: Dan Fellin
    Title: Test Drive
    Class: CS133 
*/

#include <stdio.h>

int main() {
  printf("Program: Test Drive\n"
    "Author: Dan Fellin\n"
    "1. Dan Fellin. G03546629\n"
    "2. In order to learn C.\n"
    "3. Formally, I've just taken one other CS course: CS162, Computer"
    "Science II. It was C++ class. A lot of the syntax is familiar."
    "On my own, I've played with Ruby and Python mainly, with a tiny"
    "smattering of Java and SQL. Also, I've written quite a few handy"
    "little bash scripts.\n"
    "4. Linux.\n"
    "5. Preferably bash, vim, and g++. IDEs seem like more trouble than they are worth.\n"
    "6. Looks like it might be a bit basic. But more programming practice is always handy!\n"
    "7. Learning C.\n"
    "8. Err, I like FOSS?\n"
    "9. Show me cool tidbits about C. Critique my programs.\n"
    "10. Cascade.\n");
return 0;
}
