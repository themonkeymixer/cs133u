/*
Author: Dan Fellin
Title: Formatted IO
Class: CS133u
Due Date: 2/05/12
Description: Reading and writing with scanf and print
*/

#include <stdio.h>
#include <string.h>
#define STR_LEN 80

int main() {
  char ch1;
  char ch2;
  int i;
  int j;
  float f;
  char str[STR_LEN+1];

  printf("Please enter the following variables then hit enter: char int char float: ");
  scanf("%c %d %c %f", &ch1, &i, &ch2, &f);
  printf("Here are the values you entered: %c %d %c %f\n", ch1, i, ch2, f);
  printf("Please enter the following variables then hit enter: string char float int char: ");
  scanf("%s %c %f %i %c", str, &ch1, &f, &i, &ch2);
  printf("Here are the values you entered: %s %c %f %d %c\n", str, ch1, f, i, ch2);
  printf("Please give me another integer: ");
  scanf("%d", &j);
  printf("%015d\n", j);
  printf("Please enter a float: ");
  scanf("%f", &f);
  printf("%15.2f\n", f);
  return 0;
}
