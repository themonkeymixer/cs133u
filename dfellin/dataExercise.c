/*
Author: Dan Fellin
Title: Data Assignment
Class: CS133u
Due Date: 1/29/12
Description: Declares variables in various formats then prints them to the screen.
*/

#include <stdio.h>
#include <math.h>
#include <limits.h>


int main() {
  char str[] =  "Hello, world! I am a string!";
  char ch = 'a';
  double inf = HUGE_VAL;
  short shorty = SHRT_MAX;
  short *p;
  p = &shorty;

  //1. Display the str
  printf("%s\n", str);
  //2. Display the char
  printf("%c\n", ch);
  printf("%d\n", ch);
  //3. Display the short value
  printf("%d\n", shorty);
  //4. Display positive infinity
  printf("%f\n", inf);
  //5. Display the value stored in the pointer 
  printf("%d\n", p);
  //6. Display the value of shorty with the pointer
  printf("%d\n", *p);
  return 0;
}

