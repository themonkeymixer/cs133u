/*
Student: Dan Fellin
Hello World demonstration program
Winter 2012 - CS 133U
*/
#include <stdio.h>
#include <stdlib.h>
int main() {
    printf("Hello World!\n");
    return 0;
}

