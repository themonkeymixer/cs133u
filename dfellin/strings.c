/*
Author: Dan Fellin
Title: Strings
Class: CS133u
Due Date: 2/12/12
Description: Manipulating Strings
*/

#include <stdio.h>
#include <string.h>
#define MAX_LEN 255

int readLine(char str[], int n);

int main() {
  char str1[MAX_LEN];
  char str2[MAX_LEN];
  char str3[MAX_LEN];
  size_t len1;
  size_t len2;

  printf("Please enter a string of %d characters or less: ", MAX_LEN - 1);
  readLine(str1, MAX_LEN);
  printf("Here is the string you entered: %s\n", str1);

  printf("Please enter a string of %d characters or less: ", MAX_LEN - 1);
  readLine(str2, MAX_LEN);
  printf("Here is the string you entered: %s\n", str2);

  len1 = strlen(str1);
  len2 = strlen(str2);

  printf("The first string is this long: %zu bytes\n", len1);
  printf("The second string is this long: %zu bytes\n", len2);

  strncpy(str3, str1, len1 / 2);
  strcat(str3, str2 + len2 / 2);
  printf("String 3 is: %s\n", str3);

  return 0;
}

int readLine(char str[], int max) {
  int ch, i = 0;
  while ((ch = getchar()) != '\n')
    if (i < max)
      str[i++] = ch;
  str [i] = '\0';
  return i;
}
