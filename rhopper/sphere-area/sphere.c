//Author: Rich Hopper
//title: area of a sphere	
// purpose:computes area of a sphere
//compile command: c
//page:34

#include <stdio.h>

int main(void) {
	float pi, radius, volume;
	pi = 3.1415962;

	printf ("Title: Volume of a Sphere\n");
	printf ("Purpose: To calculate the volume of a sphere accurate to 6 decimal places\n");
	printf ("Author: Rich Hopper\n");
	printf ("The vaue of pi used in this equation is 3.1415962\n");
	printf (" Note: The output of this program will be in your units cubed\n");
	printf ("Please input the value of the radius, note that the raduis is d/2\n");
	scanf ("%f" , &radius);
	printf ("that is %f units\n", radius);
	volume = (4.0f / 3.0f) * pi * (radius * radius * radius);
	printf ("Volume is %f", volume);
 	printf (" units cubed\n");
	return 0;
}
