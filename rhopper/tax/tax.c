// Author Rich Hopper
// Title  Tax Calculator
// Purpose To calculate tax on a purchase
// Commit Code gcc -Wall tax.c -o tax-app
# include <stdio.h>
int main (void){
	float purchase, plustax;
	printf (" Enter purchase amount ");
	scanf ("%f", &purchase);
	plustax = purchase * 1.05;
	printf ("the purchase with tax is $%.2f\n", plustax);
	return 0;
}
