//author           Rich Hopper
//title            polynomial calculator
// purpose         second program
//compile command  gcc -Wall polynomial.c -o polynomial-app

#include <stdio.h>

int main(void) { 
	int height, length, width, volume, dem_weight;

	printf("Entert height of box: ");
	scanf("%d", &height);
	printf("Enter lenghth of box: ");
	scanf("%d", &length);
	printf("Enter width of box: ");
	scanf("%d", &width);
	volume = height * length * width;
	dem_weight = (volume + 165) / 166;
	printf("Dimensions: %dx%dx%d\n", length, width, height);

	printf("Volume (cubic inches)= %d\n", volume);
	printf("Dimensional weight (pounds)= %d\n", dem_weight);
 
	return 0;
}
  
